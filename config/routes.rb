Rails.application.routes.draw do
  devise_for :users

  root "searches#index"

  resources :searches, only: [:index, :new, :create, :show]
end
