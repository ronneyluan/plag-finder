class CreateSearches < ActiveRecord::Migration[5.2]
  def change
    create_table :searches do |t|
      t.string :file
      t.integer :phrases_size
      t.integer :phrases_count
      t.decimal :text_percentage
      t.string :text_area
      t.integer :user_id, index: true, foreign_key: true

      t.timestamps
    end
  end
end
