class CreateSearchResults < ActiveRecord::Migration[5.2]
  def change
    create_table :search_results do |t|
      t.string :phrase
      t.string :links, array: true
      t.belongs_to :search, index: true
      t.timestamps
    end
  end
end
