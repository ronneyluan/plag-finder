module SearchHelper
  def text_percentage_options_collection
    Search.text_percentages.map { |key, value| ["#{key} %", key] }
  end

  def text_area_options_collection
    Search.text_areas.map { |key, value| [I18n.t("search.text_area.#{key}"), key] }
  end

  def links_count_label(links_count)
    if links_count  == 0
      ""
    elsif links_count == 1
      " - 1 Link"
    else
      " - #{links_count} Links"
    end
  end
end
