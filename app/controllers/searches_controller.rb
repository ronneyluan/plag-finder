class SearchesController < ApplicationController
  def index
    @searches = Search.order(updated_at: :desc).page(params[:page])
  end

  def new
    @search = Search.new(text_percentage: '50', text_area: 'beginning')
  end

  def create
    @search = Search.new(search_params)
    if @search.save
      create_search_results
      redirect_to searches_url, notice: I18n.t('searches.create.success')
    else
      render :new
    end
  end

  def show
    @search = Search.find(params[:id])
  end

private

  SERACH_PROCESSORS = [TextProcessors::ReferenceRemover,
    TextProcessors::TextReducer]

  def search_params
    params.require(:search).permit(:phrases_count, :phrases_size, :phrases_size,
      :text_percentage, :text_area, :file)
  end

  def processed_text
    search_params.delete(:file)

    SERACH_PROCESSORS.reduce(@search.decoded_file_content) do |text, processor|
      processor.new(text: text, options: search_params).perform
    end
  end

  def phrases_picker
    @phrases_picker ||= PhrasesPicker.new(text: processed_text, options: {
      phrases_count: @search.phrases_count, phrases_size: @search.phrases_size })
  end

  def create_search_results
    phrases_picker.perform.each do |phrase|
      links = FindLinks.new(phrase: phrase).from_azure

      SearchResult.create(phrase: phrase, search: @search, links: links)

      # Para evitar que exceda o limite máximo de requisições por segundo
      sleep 0.2
    end
  end
end
