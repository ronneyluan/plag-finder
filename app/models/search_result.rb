class SearchResult < ApplicationRecord
  belongs_to :search

  validates_presence_of :search, :phrase
end
