class Search < ApplicationRecord
  has_one_attached :file, dependent: :destroy
  has_many :search_results, dependent: :destroy

  enum text_area: { beginning: 'beginning', middle: 'middle', ending: 'ending' }
  enum text_percentage: (1..10).map { |i| "#{i * 10}" }

  validates_presence_of :phrases_size, :phrases_count, :text_area, :text_percentage

  validate :has_file_attached, :file_has_valid_type

  def decoded_file_content
    case file.content_type
    when 'text/plain'
      decode_content(file.download)
    when 'application/pdf'
      decode_content(pdf_file_content)
    end
  end

  def links_count
    search_results.sum { |result| result.links.count }
  end

  private

  def pdf_file_content
    temp_file = Tempfile.new('file.pdf')
    temp_file.binmode
    temp_file.write(file.blob.download)
    reader = PDF::Reader.new(temp_file)
    content = reader.pages.map(&:text).join("\n")
    temp_file.unlink
    content
  end

  def decode_content(content)
    content.encode(Encoding::UTF_8, "Windows-1252")
  rescue Encoding::UndefinedConversionError => e
    content.force_encoding(Encoding::UTF_8)
  end

  def has_file_attached
    unless file.attached?
      errors.add(:file, I18n.t('search.errors.file.empty'))
    end
  end

  def file_has_valid_type
    return unless file.attached?

    unless file.attachment.blob.content_type.in?(%w(text/plain application/pdf))
      errors.add(:file, I18n.t('search.errors.file.type'))
    end
  end
end
