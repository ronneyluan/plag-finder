$(document).on('turbolinks:load', function() {

  $('input[type=submit]').click(function() {
    $('.lds-ring').removeClass('hidden');
    $('.page-content').addClass('loading');
  });

  $('.copy-phrase').click(function() {
    var content = $(this).parent().find('.is-phrase').html();
    copyToClipboard(content);
  });

  $('.copy-link').click(function() {
    var content = $(this).parent().find('a').html();
    copyToClipboard(content);
  });

  function copyToClipboard(content) {
    var el = document.createElement('textarea');
    el.value = content.trim();
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  }
});
