module SearcherWrappers
  class Azure
    include HTTParty
    base_uri 'https://api.cognitive.microsoft.com/bing/v7.0'

    SEARCH_PATH = '/search'

    def initialize(access_token:)
      @access_token = access_token
    end

    def search(term)
      result = self.class.get(SEARCH_PATH, { headers: headers, query: { q: term } })
      JSON.parse(result.body).dig('webPages', 'value')
    end

    private

    def headers
      { 'Ocp-Apim-Subscription-Key' => @access_token }
    end
  end
end
