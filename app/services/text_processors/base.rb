module TextProcessors
  class Base
    def initialize(text:, options: default_options)
      @text = text
      @options = options
    end

    def perform
      raise NotImplementedError
    end

  protected

    def default_options
      {}
    end
  end
end
