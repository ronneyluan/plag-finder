module TextProcessors
  class ReferenceRemover < TextProcessors::Base

    def perform
      "#{new_text_matrix.join('.')}."
    end

  protected

    DEFAULT_REFERENCE_PATTERN = /[(][A-ZÀ-Ú]*[,][ ]?\d*[)]/

    def new_text_matrix
      @text.split('.').delete_if do |phrase|
        DEFAULT_REFERENCE_PATTERN.match(phrase)
      end
    end
  end
end
