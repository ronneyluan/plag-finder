module TextProcessors
  class TextReducer < TextProcessors::Base
    def perform
      phrases_array[array_range].join(".")
    end

  private

    def phrases_array
      @phrases_array ||= @text.split(".")
    end

    def new_phrases_array_size
      @new_phrases_array_size ||= (phrases_array.size.to_f * @options[:text_percentage].to_i / 100).ceil
    end

    def array_range
      case @options[:text_area]
        when "beginning" then beginning_text_range
        when "middle" then middle_text_range
        when "ending" then ending_text_range
      end
    end

    def beginning_text_range
      0..(new_phrases_array_size-1)
    end

    def middle_text_range
      middle_index = (phrases_array.size / 2).floor
      new_phrases_middle_index = (new_phrases_array_size / 2).floor
      start = middle_index - new_phrases_middle_index
      ending = start + new_phrases_array_size - 1
      start..ending
    end

    def ending_text_range
      (phrases_array.size - new_phrases_array_size)..(phrases_array-1)
    end
  end
end
