class PhrasesPicker
  def initialize(text:, options:)
    @text = text
    @options = options
    @selected_phrases = Set.new
  end

  def perform
    while @selected_phrases.size < @options[:phrases_count] do
      index = rand(text_matrix.count)
      @selected_phrases.add(text_matrix[index].join(' '))
    end

    @selected_phrases.to_a
  end

  def text_matrix
    @text_matrix ||= @text.split(' ').each_slice(@options[:phrases_size]).to_a
  end
end
