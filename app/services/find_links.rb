class FindLinks
  def initialize(phrase:)
    @phrase = phrase
  end

  def from_azure
    client = SearcherWrappers::Azure.new(access_token: ENV['AZURE_KEY'])
    results = client.search(%|"#{@phrase}"|)
    results ? results.map { |result| result['url'] } : []
  end
end
