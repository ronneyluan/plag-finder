require 'rails_helper'

RSpec.describe Search, type: :model do
  it { should validate_presence_of(:phrases_count) }
  it { should validate_presence_of(:phrases_size) }
  it { should validate_presence_of(:text_area) }
  it { should validate_presence_of(:text_percentage) }
  it { should have_many(:search_results) }

  let(:search) do
    Search.new(file: fixture_file_upload('spec/data/dummy_text.txt'),
      phrases_count: 3, phrases_size: 5, text_area: 'beginning',
      text_percentage: '50')
  end

  describe "#links_count" do
    before do
      SearchResult.create(phrase: 'phrase 1', search: search, links: ['a', 'b'])
      SearchResult.create(phrase: 'phrase 2', search: search, links: ['c'])
    end

    it "returns the sum of links count of its results" do
      expect(search.links_count).to eq(3)
    end
  end

  describe "#decoded_file_content" do
    let(:search) do
      Search.new(file: fixture_file_upload('spec/data/introdução.txt'),
        phrases_count: 3, phrases_size: 5, text_area: 'beginning',
        text_percentage: '50')
    end

    it "returns the content as UTF-8" do
      expect(search.decoded_file_content.encoding).to eq(Encoding::UTF_8)
    end

    context 'when the file encode is Windows-1252' do
      let(:search) do
        Search.new(file: fixture_file_upload('spec/data/frases-de-exemplo.txt'),
          phrases_count: 3, phrases_size: 5, text_area: 'beginning',
          text_percentage: '50')
      end

      it "returns the content as UTF-8" do
        expect(search.decoded_file_content.encoding).to eq(Encoding::UTF_8)
      end
    end
  end
end
