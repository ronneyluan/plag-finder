require 'rails_helper'

RSpec.describe SearchResult, type: :model do
  it { should belong_to(:search) }
  it { should validate_presence_of(:search) }
  it { should validate_presence_of(:phrase) }
end
