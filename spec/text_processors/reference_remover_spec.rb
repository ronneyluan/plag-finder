require 'spec_helper'

RSpec.describe TextProcessors::ReferenceRemover do
  let(:text) do
    "O primeiro passo no combate é a identificação do problema por parte dos primeiros responsáveis pela revisão do texto, em geral o professor ou o orientador.
    Pelo conhecimento que têm do aluno, os professores podem desconfiar e, diante disso, verificar de forma mais atenta o trabalho (ARAÚJO, 2017)."
  end

  subject { described_class.new(text: text) }

  describe '#perform' do
    let(:removed_piece) do
      "Pelo conhecimento que têm do aluno, os professores podem desconfiar e, diante disso, verificar de forma mais atenta o trabalho (ARAÚJO, 2017)."
    end

    it 'removes all phrases that contains a reference eg (BEZERRA, 2018)' do
      expect(subject.perform).to_not include(removed_piece)
    end

    context 'when there are not references occurencies in the given text' do
      let(:text) do
        "O primeiro passo no combate é a identificação do problema por parte dos primeiros responsáveis pela."
      end

      it 'returns the same text' do
        expect(subject.perform).to eq(text)
      end
    end
  end
end
