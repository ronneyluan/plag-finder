require 'spec_helper'

RSpec.describe TextProcessors::TextReducer do
  let(:text) { File.read('spec/data/dummy_text.txt') }
  let(:options) do
    { text_percentage: 50, text_area: 'beginning' }
  end

  subject { described_class.new(text: text, options: options) }

  describe "#perform" do
    it 'reduces the text to the given percentage in the given area'
  end
end
