require 'spec_helper'

RSpec.describe PhrasesPicker do
  describe '#perform' do
    let(:text) do
      File.read('spec/data/dummy_text.txt').gsub('\n', '')
    end
    let(:options) do
      { phrases_count: 5, phrases_size: 5 }
    end

    subject { described_class.new(text: text, options: options) }

    it 'picks the correct number of phrases' do
      expect(subject.perform.count).to eq(5)
    end

    it 'picks phrases with the correct number of words' do
      subject.perform.each do |phrase|
        expect(phrase.split(' ').count).to eq(5)
      end
    end

    it 'picks phrases from the given text' do
      subject.perform.each do |phrase|
        phrase.split(' ').each do |word|
          expect(text).to include(word)
        end
      end
    end
  end
end
